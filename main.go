package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
	"github.com/mholt/archiver/v3"
)

var header0 = `<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <title>Katalog</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/package/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css" rel="stylesheet" />
        <link href="/package/dist/digi-arbetsformedlingen/fonts/src/fonts.css" rel="stylesheet" />
		<style>
			.icon {
			display:inline;
			}
			.sc-digi-icon-file-document-h svg.sc-digi-icon-file-document {
    			color: var(--digi--icon--color);
    			max-width: var(--digi--icon--width);
    			max-height: var(--digi--icon--height);
    			width: var(--digi--icon--width, auto);
    			height: 25px;
			}
			.sc-digi-icon-archive-outline-h svg.sc-digi-icon-archive-outline {
    			color: var(--digi--icon--color);
    			max-width: var(--digi--icon--width);
    			max-height: var(--digi--icon--height);
    			width: var(--digi--icon--width, auto);
    			height: 25px;
			}
		</style>
        <script type="module" src="/package/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.esm.js"></script>

    </head>
<body>
<digi-layout-block af-variation="primary">

<digi-header
        af-system-name="Data"
        af-hide-system-name="false"
        af-menu-button-text="Meny">
        <a slot="header-logo" aria-label="Data startsida" href="/"></a>
    <div slot="header-content">
      <digi-link af-href="/swagger" title="Swagger">Swagger</digi-link>
      &nbsp;&nbsp;
      <digi-link af-href="/rss/datajobtechdevse.xml" title="RSS">RSS</digi-link>
      &nbsp;&nbsp;
      <digi-link af-href="https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-s3-to-static-web" title="Code">Code</digi-link>
      &nbsp;&nbsp;
      <digi-link af-href="https://designsystem.arbetsformedlingen.se/" title="Designsystem">Designsystem</digi-link>


    </div>
</digi-header>
`

var header1 = `<digi-table>
<table id="list">
  <thead>
    <tr>

		<th style="width:15%"><a href="?C=N&O=A">Filnamn</a>&nbsp;<a href="?C=N&O=D">&nbsp;&darr;&nbsp;</a></th>
		<th style="width:35%"><a href="?C=M&O=A">Beskrivning</a>&nbsp;<a href="?C=M&O=D">&nbsp;&darr;&nbsp;</a></th>
		<th style="width:5%"><a href="?C=S&O=A">Filstorlek</a>&nbsp;<a href="?C=S&O=D">&nbsp;&darr;&nbsp;</a></th>
		<th style="width:10%"><a href="?C=M&O=A">Datum</a>&nbsp;<a href="?C=M&O=D">&nbsp;&darr;&nbsp;</a></th>

    </tr>
  </thead>
`
var container = `
	{{ if ne .Header ""}}
		<digi-layout-container af-no-gutter>
			<digi-typography-heading-jumbo
				af-text="{{.Header}}"
				af-level="h1">
			</digi-typography-heading-jumbo>
				<digi-typography-meta af-variation="medium">						
					<p>
						{{.Text}}
					</p>

				</digi-typography-meta>
		</digi-layout-container>
	{{end}}
	`
var footer = `</table></digi-table>
</digi-layout-block>
<digi-footer af-variation="large">
  <div slot="content-top">
    <div>
      <digi-footer-card af-type="icon">
        <ul>
          <li>
            <a href="https://jobtechdev.se/sv/accessibility">
              <digi-icon-accessibility-universal></digi-icon-accessibility-universal>
              Tillgänglighetsredogörelse
            </a>
          </li>
        </ul>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card af-type="icon" >
	  <a href="https://arbetsformedlingen.se/om-webbplatsen/oppna-data">
              <digi-icon-external-link-alt></digi-icon-external-link-alt>
              Öppna data
	  </a>

      </digi-footer-card>
    </div>

  </div>
  <div slot="content-bottom-left">
    <a href="https://arbetsformedlingen.se">
      <digi-logo af-variation="large" af-color="secondary"></digi-logo>
    </a>
  </div>
  <div slot="content-bottom-right">
    <p>Följ oss på</p>
    <a href="https://www.facebook.com/Arbetsformedlingen">Facebook</a>
    <a href="https://www.youtube.com/Arbetsformedlingen">Youtube</a>
    <a href="https://www.linkedin.com/company/arbetsformedlingen">Linkedin</a>
    <a href="https://www.instagram.com/arbetsformedlingen">Instagram</a>
  </div>
</digi-footer>
<script type="text/javascript">
(function(window, document, dataLayerName, id) {
window[dataLayerName]=window[dataLayerName]||[],window[dataLayerName].push({start:(new Date).getTime(),event:"stg.start"});var scripts=document.getElementsByTagName('script')[0],tags=document.createElement('script');
function stgCreateCookie(a,b,c){var d="";if(c){var e=new Date;e.setTime(e.getTime()+24*c*60*60*1e3),d="; expires="+e.toUTCString();f="; SameSite=Strict"}document.cookie=a+"="+b+d+f+"; path=/"}
var isStgDebug=(window.location.href.match("stg_debug")||document.cookie.match("stg_debug"))&&!window.location.href.match("stg_disable_debug");stgCreateCookie("stg_debug",isStgDebug?1:"",isStgDebug?14:-1);
var qP=[];dataLayerName!=="dataLayer"&&qP.push("data_layer_name="+dataLayerName),isStgDebug&&qP.push("stg_debug");var qPString=qP.length>0?("?"+qP.join("&")):"";
tags.async=!0,tags.src="https://af.piwik.pro/containers/"+id+".js"+qPString,scripts.parentNode.insertBefore(tags,scripts);
!function(a,n,i){a[n]=a[n]||{};for(var c=0;c<i.length;c++)!function(i){a[n][i]=a[n][i]||{},a[n][i].api=a[n][i].api||function(){var a=[].slice.call(arguments,0);"string"==typeof a[0]&&window[dataLayerName].push({event:n+"."+i+":"+a[0],parameters:[].slice.call(arguments,1)})}}(i[c])}(window,"ppms",["tm","cm"]);
})(window, document, 'dataLayer', 'fe5dfccb-7911-4a60-bf2b-a431140d1846');
</script>
</digi-layout-block>
</body>
</html>
`

type Pages struct {
	Pages []Page `json:"pages"`
}

type About struct {
	About Text `json:"about"`
}

type Text struct {
	Header string `json:"header"`
	Text   string `json:"text"`
}

// Text struct which contains a name
// a short description and a long description

type Page struct {
	Name              string `json:"name"`
	Short_description string `json:"short_description"`
	Long_description  string `json:"long_description"`
}

func upload(sess *session.Session, bucket string, dest string, content string) error {
	uploader := s3manager.NewUploader(sess)

	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String(dest),
		Body:        bytes.NewReader([]byte(content)),
		ContentType: aws.String("text/html"),
	})

	if err != nil {
		return err
	}

	return nil
}

func storeLocal(dest string, content string, localStorageRoot string) {
	dirname := filepath.Dir(dest)
	destDir := filepath.Join(localStorageRoot, dirname)

	if err := os.MkdirAll(destDir, 0755); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot create directory: %v\n", err)
		return
	}

	destFile := filepath.Join(destDir, filepath.Base(dest))
	file, err := os.Create(destFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cannot create file: %v\n", err)
		return
	}
	defer file.Close()

	if _, err := file.Write([]byte(content + "\n")); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot write to file: %v\n", err)
		return
	}
}

func appendToList(original []string, element string) []string {
	// Make a copy of the original slice.
	newList := make([]string, len(original))
	copy(newList, original)

	// Append the new element to the copy.
	newList = append(newList, element)

	return newList
}

func mdToHTML(md []byte) []byte {
	// create markdown parser with extensions
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	// create HTML renderer with extensions
	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)

	return markdown.Render(doc, renderer)
}

func generateAbout(about About, str string) string {
	aboutTmp, err := template.New(str).Parse(str)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cannot parse template", err)
	}

	var templateBytes bytes.Buffer
	err = aboutTmp.Execute(&templateBytes, map[string]interface{}{
		"Header": about.About.Header,
		"Text":   string(mdToHTML([]byte(about.About.Text))[:]),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cannot execute template", err.Error())
	}
	return templateBytes.String()
}

func procDir(sess *session.Session, localStorageRoot string, bucket string, mode string, dirList []string) int {

	// dirList is a list of directory names, e.g. ["dir1", "dir2", "dir3"]
	// Here we create a string representation of the directory path, e.g. "dir1/dir2/dir3/"
	dir := ""
	if len(dirList) > 0 {
		dir = strings.Join(dirList, "/") + "/"
	}

	//fmt.Fprintf(os.Stderr, "entered dir %s\n", dir)

	resp := listObjects(sess, bucket, dir)
	var pages Pages
	var about About
	var pagesPath = dir + "pages.json"

	requestInput := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(pagesPath),
	}
	svc := s3.New(sess)
	result, err := svc.GetObject(requestInput)
	if err != nil {
		fmt.Println("Could not find ", dir, "pages.json")
	} else {
		file, err := ioutil.ReadAll(result.Body)
		if err == nil {

		}
		json.Unmarshal(file, &pages)
		json.Unmarshal(file, &about)
	}

	if resp == nil || (len(resp.Contents) == 0 && len(resp.CommonPrefixes) == 0) {
		//fmt.Fprintf(os.Stderr, "No items in bucket %q\n", bucket)
		return 0
	}

	// Create the HTML for the directory listing, as a list of HTML fragments,
	dirListHTML := []string{}

	dirListHTML = append(dirListHTML, header0)

	dirListHTML = buildBreadcrumbLinks(dir, dirListHTML, dirList)

	dirListHTML = append(dirListHTML, header1, generateAbout(about, container))

	dirListHTML = append(dirListHTML, "<tbody>") // This is a bit messy and a consequence of the way the HTML is built up. A better solution would be to use a template engine - future work.

	itemCount := 0

	// This loop iterates through the DIRECTORY ITEMS in the directory.
	for _, prefix := range resp.CommonPrefixes {
		name := *prefix.Prefix
		basename := filepath.Base(name)
		if isBlacklisted(basename) {
			continue
		}

		dirItemCount := procDir(sess, localStorageRoot, bucket, mode, appendToList(dirList, basename))

		if dirItemCount > 0 {
			var match Page
			for i := 0; i < len(pages.Pages); i++ {
				if basename == pages.Pages[i].Name {
					match = pages.Pages[i]

					break
				}
			}
			// Mess etc
			row := fmt.Sprintf(`
			<tr>
				<td class="link ">
					<digi-icon-archive-outline class="icon "></digi-icon-archive-outline>
					<a href="%s/index.html" title="%s">%s</a>
				</td>
				<td>%s</td>
				<td class="size">-</td>
				<td class="date"></td>

			</tr>
			`, basename, basename, basename, match.Short_description)
			dirListHTML = append(dirListHTML, row)
			itemCount += dirItemCount + 1
			fmt.Fprintf(os.Stderr, "directory item: name: %s (%s)\n", basename, name)

		} else {
			fmt.Fprintf(os.Stderr, "Skipping empty directory %s\n", basename)
		}
	}

	// This loop iterates through the FILE ITEMS in the directory.
	for _, item := range resp.Contents {
		name := *item.Key
		name = filepath.Base(name)

		if isBlacklisted(name) || strings.HasSuffix(name, ".dcat.json") {
			continue
		}
		date := item.LastModified.String()
		size := fmt.Sprintf("%d", *item.Size)

		if *item.Size == 0 {
			fmt.Fprintf(os.Stderr, "Skipping empty file %s\n", name)
			continue
		}

		// More mess - this will be more elegant when we use a template engine
		row := fmt.Sprintf(`
		<tr>
			<td class="link">
				<digi-icon-file-document class="icon"></digi-icon-file-document>
				<a href="%s" title="%s">%s</a>
			</td>
			<td class="description"></td>
			<td class="size">%s</td>
			<td class="date">
				<digi-typography-time
						af-variation="primary"
						af-date-time=%s
				</digi-typography-time>
			</td>
		</tr>
		`, name, name, name, size, date)

		//fmt.Fprintf(os.Stderr, "file item: name: %s, size %s, date %s\n", name, size, date)
		dirListHTML = append(dirListHTML, row)
		itemCount++
	}

	// Add the last fragments of the HTML
	dirListHTML = append(dirListHTML, "</tbody>")
	dirListHTML = append(dirListHTML, footer)

	// Store the HTML in S3 or locally
	if dir != "" {
		if mode == "local" {
			storeLocal(dir+"index.html", strings.Join(dirListHTML, "\n"), localStorageRoot)
		} else {
			upload(sess, bucket, dir+"index.html", strings.Join(dirListHTML, "\n"))
		}
	} else {
		fmt.Fprintf(os.Stderr, "Skipping index.html in root directory\n")
	}

	return itemCount
}

func listObjects(sess *session.Session, bucket string, dir string) *s3.ListObjectsV2Output {
	svc := s3.New(sess)
	var result s3.ListObjectsV2Output
	var continuationToken *string

	for {
		input := &s3.ListObjectsV2Input{
			Bucket:            aws.String(bucket),
			Prefix:            aws.String(dir),
			Delimiter:         aws.String("/"),
			ContinuationToken: continuationToken,
		}

		resp, err := svc.ListObjectsV2(input)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to list items in bucket %q: %v\n", bucket, err)
			break
		}

		result.Contents = append(result.Contents, resp.Contents...)
		result.CommonPrefixes = append(result.CommonPrefixes, resp.CommonPrefixes...)

		if resp.IsTruncated != nil && *resp.IsTruncated {
			continuationToken = resp.NextContinuationToken
		} else {
			break
		}
	}

	return &result
}

func buildBreadcrumbLinks(dir string, dirListHTML []string, dirList []string) []string {
	if dir != "" {
		dirListHTML = append(dirListHTML, strings.Replace(`<digi-navigation-breadcrumbs af-current-page="{DIR}">`, "{DIR}", dirList[len(dirList)-1], -1))
		dirListHTML = append(dirListHTML, `<a href="/">Start</a>`)
		for i := 0; i < len(dirList)-1; i++ {
			_dir := dirList[i]
			dirListHTML = append(dirListHTML, `<a href="/`+strings.Join(dirList[:i+1], "/")+`">`+_dir+`</a>`)
		}
		dirListHTML = append(dirListHTML, `</digi-navigation-breadcrumbs>`)
	} else {
		dirListHTML = append(dirListHTML, strings.Replace(`<digi-navigation-breadcrumbs af-current-page="{DIR}">`, "{DIR}", "Start", -1))
		dirListHTML = append(dirListHTML, `</digi-navigation-breadcrumbs>`)
	}
	return dirListHTML
}

func isBlacklisted(name string) bool {
	blacklist := []string{
		"page.json",
		"pages.json",
		"index.html",
		"public-tokens",
		"dcatotron",
		"Nginx-Fancyindex-Theme-light",
		"package",
		"assets",
		"swagger",
		"rss",
		"50x.html",
		"README",
		"directory.dcat.json",
		"collection.dcat.json",
		"expediering",
		".well-known",
		"presentationer",
		"rapporter",
		"data",
	}

	for _, b := range blacklist {
		if b == name {
			return true
		}
	}

	return false
}

func main() {
	defaultBucket := os.Getenv("BUCKET")
	if defaultBucket == "" {
		defaultBucket = "data.jobtechdev.se-adhoc-test" // Default value if BUCKET env var is not set
	}

	defaultRegion := os.Getenv("REGION")
	if defaultRegion == "" {
		defaultRegion = "eu-central-1" // Default value if REGION env var is not set
	}

	// Define CLI arguments
	bucketPtr := flag.String("bucket", defaultBucket, "The name of the S3 bucket")
	regionPtr := flag.String("region", defaultRegion, "The AWS region")

	localDestDir := flag.String("dest", "", "The local destination directory")

	modePtr := flag.String("mode", "s3", "The mode to run in, either 's3' or 'local'")

	// Parse CLI arguments
	flag.Parse()

	// Use the CLI arguments
	bucket := *bucketPtr
	region := *regionPtr
	mode := *modePtr

	if mode != "s3" && mode != "local" {
		fmt.Fprintf(os.Stderr, "Invalid mode: %s\n", mode)
		os.Exit(1)
	}

	var localStorageRoot string = "/tmp"

	if mode == "local" {
		if *localDestDir == "" {
			var err error
			localStorageRoot, err = os.MkdirTemp("/tmp", "static-web-*")
			if err != nil {
				panic(err)
			}
		} else {
			localStorageRoot = *localDestDir

			if _, err := os.Stat(localStorageRoot); !os.IsNotExist(err) {
				fmt.Fprintf(os.Stderr, "Local destination directory %s already exists - remove first\n", localStorageRoot)
				os.Exit(1)
			}
		}
		fmt.Fprintf(os.Stderr, "Running in local mode, storing in directory %s\n", localStorageRoot)
	}

	fmt.Fprintf(os.Stderr, "Bucket: %s, Region: %s\n", bucket, region)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create session: %v\n", err)
		os.Exit(1)
	}

	procDir(sess, localStorageRoot, bucket, mode, []string{})

	if mode == "local" {
		fmt.Fprintf(os.Stderr, "Local destination directory: %s\n", localStorageRoot)
		copyAssetsToLocalIfNeeded(localStorageRoot)
		serveLocalDir(localStorageRoot)
	}
}

func copyAssetsToLocalIfNeeded(localStorageRoot string) {
	assetsDir := filepath.Join(localStorageRoot, "assets")
	if _, err := os.Stat(assetsDir); os.IsNotExist(err) {
		assetsZip := filepath.Join("assets", "assets.zip")
		unzipErr := archiver.Unarchive(assetsZip, localStorageRoot)
		if unzipErr != nil {
			fmt.Fprintf(os.Stderr, "Failed to unzip assets: %v\n", unzipErr)
			os.Exit(1)
		}
		if unzipErr != nil {
			fmt.Fprintf(os.Stderr, "Failed to unzip assets: %v\n", unzipErr)
			os.Exit(1)
		}
	}
}

func serveLocalDir(dir string) {
	http.Handle("/", http.FileServer(http.Dir(dir)))
	log.Println("Listening on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
