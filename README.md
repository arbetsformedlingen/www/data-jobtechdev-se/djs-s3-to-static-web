<!--
SPDX-FileCopyrightText: 2023 Arbetsförmedlingen JobTech - The Swedish Public Employment Service's Investment in JobTech Development

SPDX-License-Identifier: CC0-1.0
-->

# Project Title

**djs-s3-to-static-web**

Traverse the directories and files in an [S3](https://aws.amazon.com/s3/) bucket and create a static web site from it.

## Table of Contents

- [Installation and Requirements](#installation-and-requirements)
- [Quickstart Instructions](#quick-start-instructions)
- [Usage](#usage)
- [Known Issues](#known-issues)
- [License](#license)
- [Maintainers](#maintainers)


## Installation and Requirements
```
go get -d -v
go build -o main
```

## Quick start instructions
First, make sure you have S3 configured, for example with a [`~/.aws/credentials`](https://docs.aws.amazon.com/keyspaces/latest/devguide/access.credentials.html#aws.credentials.manage) file.

The program can operate in two modes: S3 (where generated html files are uploaded to the bucket), or local (where the files are stored locally).

The program accepts the following arguments:
```
./main -bucket <YourBucketName>  -region <YourRegion>  -mode <s3|local>  -dest <local destination directory>
```
By default, the program uses bucket `data.jobtechdev.se-adhoc-test` in region `eu-central-1`, so you only need supply those flags if you need to override these values.

Here is how you run the program in local mode and write the result to directory `/tmp/gensite` (which should not already exist):
```
./main -mode local -dest /tmp/gensite
```
At the end of the output, a URL is printed which is served by a simple web server to facilitate local development. You use it with your web browser to view the generated site.

And here is how you run the program in S3 mode:
```
./main -mode s3
```
S3 mode requires AWS credentials with write permission to the bucket.


## Environment variables
These are mainly useful when running the program in Kubernetes/Openshift, to set the configuration.

- `BUCKET` - this provides a value for the S3 bucket.
- `REGION` - this provides a value for the S3 region.

## Known issues


## License

This project is licensed under the Apache License Version 2.0 - see
the [LICENSE](LICENSE.txt) file for details

----

## See also
The assets are copied from [Arbetsförmedlingen's Designsystem](https://designsystem.arbetsformedlingen.se/) ([Gitlab](https://gitlab.com/arbetsformedlingen/designsystem/digi)) under Apache version 2.0.


## Maintainers

See [CODEOWNERS](CODEOWNERS).
