FROM docker.io/library/golang:1.24.1-alpine3.21 AS builder

WORKDIR $GOPATH/src/mypackage/myapp/
COPY go.mod main.go go.sum ./
RUN go get -d -v &&\
    go build -o /go/bin/main



############################
FROM scratch
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /go/bin/main /go/bin/main
ENTRYPOINT ["/go/bin/main"]
